<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('playlists', 'PlaylistController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Ajout de louis */
Route::get('playlists', 'PlaylistController@index')->name('playlists');

Route::get('show', 'PlaylistController@show')->name('show');

Route::get('modify', 'UserController@modify_view');

Route::post('profile', 'UserController@update_avatar');

Route::get('/admin', 'AdminController@index')->name('homeAdmin');

Route::resource('search', 'SearchController');

Route::get('/search', 'SearchController@index')->name('search');

Route::post('/search', 'SearchController@search')->name('search');

Route::resource('upload', 'UploadController');
//Route::get('/upload', 'UploadController@uploadForm')->name('upload');
//
//Route::post('/upload', 'UploadController@uploadSubmit')->name('upload');



/* Ajout de Antoine */
Route::resource('albums', 'AlbumController');

Route::get('albums', 'AlbumController@index')->name('albums');

Route::get('show', 'AlbumController@show')->name('show');

Route::resource('artists', 'ArtistController');

Route::get('artists', 'ArtistController@index')->name('artists');

Route::get('show', 'ArtistController@show')->name('show');

Route::resource('musics', 'MusicController');

Route::get('musics', 'MusicController@index')->name('musics');

Route::get('show', 'MusicController@show')->name('show');

Route::post('destroy', 'MusicController@delete')->name('delete');
