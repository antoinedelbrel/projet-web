<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($user) {
            $user->assignRole('artists');
            $user->artist()->save(factory(App\Artist::class)->make());
        });
        factory(App\User::class, 10)->create()->each(function ($user) {
            $user->assignRole('client');
        });
    }
}
