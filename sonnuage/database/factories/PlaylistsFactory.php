<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Playlist;
use Faker\Generator as Faker;



$factory->define(Playlist::class, function (Faker $faker) {
    $types_of_musics = array("Hip-Hop", "Electro", "Pop", "RnB", "Hard Bass", "Latino", "Jazz", "Classique", "Afro", "Soul", "K-pop", "Funk", "Country", "Blues", "Punk", "Folk & Acoustique");
    shuffle($types_of_musics);
    return [
        //'name', 'description', 'music_id', 'owner_id'
        'name' => $types_of_musics[0],
        'description' => $faker->text(200),
        'music_id' => rand(1, 10),
        'owner_id' => mt_rand(1, 4),
    ];
});
