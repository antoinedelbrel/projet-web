<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Artist;
use Faker\Generator as Faker;

$factory->define(Artist::class, function (Faker $faker) {
    $artists = array("Vald", "Stromae", "Nekfeu", "Niska");
    shuffle($artists);
    return [
        'pseudo' => $artists[0],
    ];
});
