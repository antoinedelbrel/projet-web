<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Music;
use Faker\Generator as Faker;

$factory->define(Music::class, function (Faker $faker) {
    $musics = array("dragon", "avf", "squa", "italia");
    shuffle($musics);
    return [
        'name' => $musics[0],
        'artist_id' => 1,
        'album_id' => rand(1, 10),
        'release_date' => $faker->date(),
    ];
});
