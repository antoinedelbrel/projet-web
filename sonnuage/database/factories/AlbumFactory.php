<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Album;
use Faker\Generator as Faker;

$factory->define(Album::class, function (Faker $faker) {
    $albums = array("Xeu", "Racine Carré", "Cyborg", "Commando");
    shuffle($albums);
    return [
        'title' => $albums[0],
        'artist_id' => rand(1, 10),
        'release_date' => date('Y-m-d',today()->getTimestamp()),
    ];
});
