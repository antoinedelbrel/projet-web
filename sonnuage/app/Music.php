<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = [
        'name', 'artist_id', 'album_id', 'cover', 'release_date'
    ];

    public function albums(){
        return $this->belongsTo('App\Album');
    }

    public function artists(){
        return $this->belongsToMany('App\Artist');
    }

    public function playlists(){
        return $this->belongsToMany('App\Playlist');
    }
}
