<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $fillable = [
        'name', 'description', 'music_id', 'owner_id',
    ];

    public function musics()
    {
        return $this->belongsToMany('App\Music');
    }
}
