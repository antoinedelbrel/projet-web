<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UploadController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('uploadForm', compact('user', $user));
    }

    public function store(Request $request)
    {
        $artist = $request->get('pseudo');
        $album = $request->get('album');
        $music_name = $request->get('music_name');

        $music_file = $request->file('music_file');
        $cover_file = $request->file('cover');

        $music_destination = public_path('storage/musics/'.$artist.'/'.$album.'/');
        $cover_destination = public_path('storage/covers/'.$artist.'/');

        $music_file_name = $music_name.'.'.$music_file->getClientOriginalExtension();
        $cover_file_name = $music_name.'-cover.'.$cover_file->getClientOriginalExtension();

        $music_file->move($music_destination, $music_file_name);
        $cover_file->move($cover_destination, $cover_file_name);


        $q_artist = DB::table('artists')->select('id', 'pseudo')->where('pseudo', 'like', $artist);
        $q_user_id = DB::table('users')->select('id', 'name')->where('name', 'like', $artist)->orderBy('id')->first();

        if ($q_artist->doesntExist()) {
            DB::table('artists')
            ->insertGetId([
                'pseudo' => $artist,
                'user_id' => intval($q_user_id->id),
            ]);
        }

        $seconde_q_artist = DB::table('artists')->select('id', 'pseudo')->where('pseudo', 'like', $artist)->orderBy('id')->first();

        DB::table('albums')
            ->insertGetId([
            'artist_id' => intval($seconde_q_artist->id),
            'title' => $album,
            'release_date' => date('Y-m-d'),
            ]);

        $third_q_artist = DB::table('artists')->select('id', 'pseudo')->where('pseudo', 'like', $artist)->orderBy('id')->first();

        $q_album = DB::table('albums')->select('id', 'artist_id', 'title')->where([
            ['artist_id', '=', intval($third_q_artist->id)],
            ['title', 'like', $album],
        ])->first();


        DB::table('musics')->insertGetId([
            'name' => $music_name,
            'artist_id' => intval($third_q_artist->id),
            'album_id' => $q_album->id,
            'cover' => $cover_file_name,
            'release_date' => date('Y-m-d'),
        ]);

        return back()->with('success', "Music uploaded successfully !");
    }
}
