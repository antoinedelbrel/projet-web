<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function modify_view() {
        $user = Auth::user();

        return view('user.modify', compact('user', $user));
    }
    public function update_avatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            ]);

        $user = Auth::user();

        $avatarName = hash('sha256',$user->id).'.jpg';

        $avatar = $request->avatar->storeAs('avatars', $avatarName);

        $user->avatar = $avatarName;

        $user->save();

        return back()->with('success', 'Image upload !');
    }
}
