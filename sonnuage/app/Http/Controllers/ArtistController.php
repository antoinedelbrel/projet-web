<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Music;
use App\Album;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artists.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Music  $artist
     * @return \Illuminate\Http\Response
     */
    //public function show(Artist $music)

    public function show(Artist $artist)
    {
        return view('artists.show', compact('artist'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        $artist->delete();

        return redirect()->route('artists')
            ->with('success', 'Artist deleted successfully');
    }
}
