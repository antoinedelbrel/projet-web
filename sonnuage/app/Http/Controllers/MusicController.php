<?php

namespace App\Http\Controllers;

use App\Music;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musics = Music::all();

        return view('musics.index', compact('musics'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    //public function show(Music $music)

    public function show(Music $music)
    {
        return view('musics.show', compact('music'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function destroy(Music $music)
    {
        $music->delete();

        return back()->with('success', 'musics deleted successfully');
    }
}
