<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Music;
use App\Playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search.search');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function search(Request $request)
    {
        $query_text = $request->input('search');

        $query = sprintf("%%%s%%", $query_text);


        // Select 2 columns

        $playlist = Playlist::where('name', 'LIKE', $query)
            ->select('name', 'description');

        $music = Music::where('name', 'LIKE', $query)
            ->leftJoin('artists', 'musics.artist_id', '=', 'artists.id')
            ->select('musics.name', 'artists.pseudo');


        $album = Album::where('title','LIKE', $query)
            ->orWhere('pseudo', 'LIKE', $query)
            ->leftJoin('artists', 'albums.artist_id', '=', 'artists.id')
            ->select('albums.title', 'artists.pseudo')
            ->union($playlist)
            ->union($music)
            ->get();


        if(count($album)>0)
            return view('search.search')->with('details', $album);
        else return view('search.search')->withMessage('Not found');

    }
}
