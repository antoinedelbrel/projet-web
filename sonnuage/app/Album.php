<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'title', 'artist_id', 'release_date'
    ];

    public function set_release_date_attribute($date)
    {
        $this->attributes['release_date'] = Carbon::parse($date);
    }

    public function musics(){
        return $this->hasMany('App\Music');
    }

    public function artists(){
        return $this->belongsTo('App\Artist');
    }
}
