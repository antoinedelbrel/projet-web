<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Artist extends Model
{
    use HasRoles;

    protected $fillable = [
        'pseudo', 'user_id'
    ];

    public function musics()
    {
        return $this->belongsToMany('App\Music');
    }

    public function albums()
    {
        return $this->hasMany('App\Album');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
