@extends('layout')

@section('content')

    <div class="container mt-4">
        <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
            <div class="pull-left">
                <h2>Faites vibrer le monde !</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('artists') }}">Retour</a>
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
        @endif

        <form action="{{ route('upload.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="text" name="pseudo" class="form-control" value="{{ $user->name ?? 'Anonymous User' }}" hidden>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Music name</strong>
                        <input class="form-control" name="music_name" placeholder="Music name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Album name</strong>
                        <input class="form-control" name="album" placeholder="Album name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group" {{ $errors->has('music_file') ? 'has-error' : '' }}>
                        <strong>Music</strong>
                        <input type="file" class="form-control" name="music_file" id="music_file">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group" {{ $errors->has('cover') ? 'has-error' : '' }}>
                        <strong>Cover</strong>
                        <input type="file" class="form-control" name="cover" id="cover">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </form>
    </div>

@endsection
