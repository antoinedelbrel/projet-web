
@extends('layout')

@section('content')
    <?php
    use Illuminate\Support\Facades\DB;
    $user = DB::table('users')
        ->select('users.id', 'artists.id', 'users.avatar')
        ->leftJoin('artists', 'artists.user_id', '=', 'users.id')
        ->where('users.id', '=', $artist->user_id)
        ->first();

    $albums = DB::table('albums')
        ->select('id', 'artist_id', 'title', 'release_date')
        ->where('artist_id', '=', $user->id)
        ->get();
    ?>
    <div class="container">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('artists') }}">Retour</a>
        </div>
        <div id="user_baniere">
            <div id="image">
                <img src="/storage/avatars/{{ $user->avatar }}" alt="{{ $artist->pseudo }} picture" />
            </div>
            <div class="text">
                <h2>{{ $artist->pseudo }}</h2>
            </div>
        </div>
        <div id="albums-list">
            <div class="text">
                <h3>Albums :</h3>
            </div>
            <ul>
                @foreach($albums as $album)
                    <li>
                        <a href="{{ route('albums.show', $album->id) }}">
                        {{ $album->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
