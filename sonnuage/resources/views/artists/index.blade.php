@extends('layout')

@section('content')
<div class="container">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Tous les Artistes</h2>
        </div>
    </div>

    <?php
    use Illuminate\Support\Facades\DB;
    $artists = DB::table('artists')->select('id', 'pseudo', 'user_id')->get();
    ?>

    <div class="group-playlists">
        @foreach($artists as $artist)
            <?php
            $user = DB::table('users')
                ->select('artists.id', 'users.avatar')
                ->leftJoin('artists', 'artists.user_id', '=', 'users.id')
                ->where('users.id', '=', $artist->user_id)
                ->first();
            ?>
            <div class="playlist-unit">
                <a href="{{ route('artists.show',$artist->id) }}">
                    <img src="/storage/avatars/{{ $user->avatar }}" alt="{{ $artist->pseudo }} picture" />
                    <h3>{{ $artist->pseudo }}</h3>
                </a>
            </div>
        @endforeach
    </div>
</div>
@endsection
