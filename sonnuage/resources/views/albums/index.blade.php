@extends('layout')

@section('content')
<div class="container">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Tout vos Albums</h2>
        </div>
    </div>

    <div class="group-playlists">
        @foreach(App\Album::select(['albums.id', 'albums.title', 'albums.artist_id', 'albums.release_date', 'artists.pseudo', 'musics.cover'])
        ->leftJoin('artists', 'albums.artist_id', '=', 'artists.id')
        ->leftJoin('musics', 'albums.id', '=', 'musics.album_id')
        ->get() as $album)
        <div class="playlist-unit">
            <a href="{{ route('albums.show',$album->id) }}">
                <img src="/storage/covers/{{  $album['pseudo'] }}/{{ $album['cover'] }}" alt="album" />
                <h3>{{ $album['title'] }}</h3>
                <p>{{ $album['artist_id'] }}</p>
                <p>{{  date_format(date_create($album['release_date']), 'Y') }}</p>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
