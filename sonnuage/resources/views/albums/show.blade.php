@extends('layout')

@section('content')
    <?php
    $artist = App\Music::select('artists.pseudo', 'musics.cover')
        ->leftJoin('artists', 'musics.artist_id', '=', 'artists.id')
        ->where([
            ['artist_id', '=', $album->artist_id],
            ['album_id', '=', $album->id],
        ])
        ->first();
    ?>
<div id="album">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('albums') }}">Retour</a>
        </div>
    </div>
    <div id="baniere">
        <div class="cover">
            <img src="/storage/covers/{{ $artist->pseudo }}/{{ $artist->cover }}" alt="{{ $album->title }}_cover" />
        </div>
        <div id="text">
            <h2>{{ $album->title }}</h2>
            <p>{{ $artist->pseudo }}</p>
            <p>{{ date_format(date_create($album->release_date), 'Y') }}</p>
        </div>
    </div>
    <div id="music-list">
        <ul>
            @foreach (App\Music::select('musics.name', 'artists.pseudo')
            ->leftJoin('artists', 'musics.artist_id', '=', 'artists.id')
            ->where([
                ['artist_id', '=', $album->artist_id],
                ['album_id', '=', $album->id],
            ])
            ->get() as $music)
            <div class="playlist-unit">
                <a href="{{ route('musics.show',$album->id) }}">
                    <li>
                    {{ $music['name'] }} - {{ $music['pseudo'] }}
                    </li>
                </a>
            </div>
            @endforeach
        </ul>
    </div>
</div>

@endsection
