@extends('layout')

@section('content')

<div class="container mt-4">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Modifiez l'album</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('albums') }}">Retour</a>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('albums.update',$album->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Titre:</strong>
                    <input type="text" name="title" value="{{ $album->title }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Date de sortie:</strong>
                    <input type="date" name="release_date" value="{{ $album->release_date }}" class="form-control" placeholder="Release_date">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Artist id:</strong>
                    <input type="text" name="artist_id" value="{{ $album->artist_id }}" class="form-control" placeholder="Artist_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Music id:</strong>
                    <input type="text" name="music_id" value="{{ $album->music_id }}" class="form-control" placeholder="music_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
</div>

@endsection