<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SONUAGE</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>

    <div id="parent">
        <header>
            <div class="container">
                <nav class="navbar navbar-expend-lg navbar-light">
                    <div id="logo_container">
                        <a class="navbar-brand" href="{{ url('/') }}">Sonuage
                            <img src="{{ asset('images/logo.png') }}" alt="sonuage_logo" />
                        </a>
                    </div>
                    <!-- <button class="bouton navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button> -->
                    <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                    <div class="liste">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="lien-nav-link" href="{{ route('albums') }}">Album</a>
                            </li>
                            <li class="nav-item">
                                <a class="lien-nav-link" href="{{ route('playlists') }}">Playlist</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('artists') }}" class="lien-nav-link">Artiste</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('musics') }}" class="lien-nav-link">Musique</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('upload.index') }}" class="lien-nav-link btn btn-success">Upload</a>
                            </li>
                            @guest
                            <li class="nav-item">
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif
                            @else
                            <li class="nav-item dropdown">
                                <div id="user-place" href="#" role="button">
                                    <div id="avatar" style="background-image: url('/storage/avatars/{{ Auth::user()->avatar }}');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            background-size: 100% 100%;
                                            ">
                                    </div>
                                </div>

                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                    <p id="username" class="dropdown-item">
                                        {{ Auth::user()->name }}
                                    </p>
                                    <a class="dropdown-item" href="{{ url('/home') }}">
                                        Home
                                    </a>

                                    <a class="dropdown-item" href="/modify">
                                        Modify Profile
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                    <!-- </div> -->
                </nav>
            </div>
        </header>
    </div>

    {{-- <div id="music-player">--}}
    {{-- <audio controls="controls">--}}
    {{-- <source src="{{ asset('storage/musics/Discovery/daft-punk-aerodynamic.mp3') }}"/>--}}
    {{-- Your browser does not support the audio element.--}}
    {{-- </audio>--}}
    {{-- </div>--}}

    <div class="content mt-4">
        @yield('content')
    </div>

    <footer>
        <p>
            Sonuage aucun droits réservés.
        </p>
        <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
            <div id="gitlab">
                <div class="pull-left">
                    <img class="w-50" src="{{ asset('images/gitlab_black.png') }}" alt="logo de gitlab" />
                    <a href="https://gitlab.com/antoinedelbrel/projet-web" target="_blank" alt="lien vers le gitlab">Repo Gitlab</a>
                </div>
            </div>
            <div class="pull-right">
                <a href="{{ route('upload.index') }}" class="lien-nav-link btn btn-success">Upload une musique</a>
            </div>
        </div>
    </footer>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

</body>

</html>