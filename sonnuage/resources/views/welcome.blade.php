<?php

use Illuminate\Support\Facades\DB; ?>
@extends('layout')

@section('content')

<div class="container">
    <h1>Bienvenu sur Sonuage !</h1>

    <h2 class="mt-4">Quelques artistes</h2>
    <div class="group-playlists">
        <div class="artiste">
            <?php $artist1 = DB::table('artists')->select('id', 'pseudo')->get() ?>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('artists.show', $artist1{0}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="artist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $artist1{0}->pseudo }}</p>
                    </div>
                </a>
            </div>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('artists.show', $artist1{0}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="artist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $artist1{1}->pseudo }}</p>
                    </div>
                </a>
            </div>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('artists.show', $artist1{0}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="artist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $artist1{2}->pseudo }}</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <h2 class="mt-4">Quelques playlists</h2>
    <div class="group-playlists">
        <div class="playlist">
            <?php $playlist1 = DB::table('playlists')->select('id', 'name', 'description')->get() ?>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('playlists.show', $playlist1{0}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $playlist1{0}->name }}</p>
                        <p>{{ $playlist1{0}->description }}</p>
                    </div>
                </a>
            </div>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('playlists.show', $playlist1{1}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $playlist1{1}->name }}</p>
                        <p>{{ $playlist1{1}->description }}</p>
                    </div>
                </a>
            </div>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('playlists.show', $playlist1{2}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $playlist1{2}->name }}</p>
                        <p>{{ $playlist1{2}->description }}</p>
                    </div>
                </a>
            </div>
        </div>

    </div>
    <h2 class="mt-4">Quelques Albums</h2>
    <div class="group-playlists">
        <div class="album">
            <?php $album1 = DB::table('albums')->select('id', 'title', 'artist_id', 'release_date')->get() ?>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('albums.show', $album1{0}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $album1{0}->id }}</p>
                        <p>{{ $album1{0}->title }}</p>
                        <p>{{ $album1{0}->artist_id }}</p>
                        <p>{{ $album1{0}->release_date }}</p>
                    </div>
                </a>
            </div>

            <div class="playlist-unit carousel-item active">
                <a href="{{ route('albums.show', $album1{1}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $album1{1}->id }}</p>
                        <p>{{ $album1{1}->title }}</p>
                        <p>{{ $album1{1}->artist_id }}</p>
                        <p>{{ $album1{1}->release_date }}</p>
                    </div>
                </a>
            </div>
            <div class="playlist-unit carousel-item active">
                <a href="{{ route('albums.show', $album1{2}->id) }}">
                    <img src="{{ asset('images/playlist_white.png') }}" class="d-block w-100" alt="playlist" />
                    <div class="caroussel-caption d-none d-md-block">
                        <p>{{ $album1{2}->id }}</p>
                        <p>{{ $album1{2}->title }}</p>
                        <p>{{ $album1{1}->artist_id }}</p>
                        <p>{{ $album1{1}->release_date }}</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection