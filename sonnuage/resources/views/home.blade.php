@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
<div class="group-playlists">
    @foreach( $play = DB::table('playlists')->select('id', 'name', 'description')->where('owner_id', '=', (string)Auth::id())->get() as $playlists_user )
        <div class="playlist-unit">
            <img src="{{ asset('images/playlist_white.png') }}" alt="playlist" />
            <h3>{{ $playlists_user->name }}</h3>
            <p>{{ $playlists_user->description }}</p>
            <form action="{{ route('playlists.destroy',$playlists_user->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('playlists.show',$playlists_user->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('playlists.edit',$playlists_user->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
    @endforeach
</div>
@endsection
