@extends ('layout')

<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <title>Search</title>
</head>
@section ('content')
<div class="container">
    <form action="{{ URL::to('/search') }}" method="POST" role="search">
        @csrf

        <input type="text" class="form-control" name="search" placeholder="Search something cool">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-search"></i>
        </button>
    </form>
</div>
@if(isset($details))
@foreach($details as $result)
<div class="search-result">
    <p>{{ $result->pseudo }}</p>
    <p>{{ $result->name }}</p>
    <p>{{ $result->title }}</p>
</div>
@endforeach

@endif
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

@endsection
