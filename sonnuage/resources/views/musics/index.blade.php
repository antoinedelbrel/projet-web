@extends('layout')

@section('content')
<div class="container">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Toutes vos musiques</h2>
        </div>
    </div>
    <div class="group-playlists">
        @foreach(App\Music::select('musics.id', 'musics.name', 'artists.pseudo', 'musics.cover')
        ->leftJoin('artists', 'musics.artist_id', '=', 'artists.id')
        ->get() as $music)

        <div class="playlist-unit">
            <a href="{{ route('musics.show', $music->id) }}">
            <img src="/storage/covers/{{ $music['pseudo'] }}/{{ $music['cover'] }}" alt="{{ $music['name'] }}_cover" />
            <h3>{{ $music['name'] }}</h3>
            <p>{{ $music['pseudo'] }}</p>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
