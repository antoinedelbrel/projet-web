@extends('layout')

@section('content')

    <?php
    $value = App\Music::select('musics.name as music_title', 'musics.cover', 'artists.pseudo as artist_name', 'albums.title as album_name')
        ->where('musics.id', '=', $music->id)
        ->leftJoin('artists', 'musics.artist_id', '=', 'artists.id')
        ->leftJoin('albums', 'musics.album_id', '=', 'albums.id')
        ->first()
    ?>

    <div id="music-player">
        <div id="cover">
            <img id="img-cover" src="/storage/covers/{{ $value->artist_name }}/{{ $value->cover }}" alt="{{ $value->music_title }} cover"/>
            <div id="text">
                <h1>{{ $value->music_title }}</h1>
                <h2>{{ $value->artist_name }}</h2>
                <h2>{{ $value->album_name }}</h2>
            </div>
        </div>
        <div class="player">
            <audio controls="controls" id="music-player">
                <source id="song_played" src="/storage/musics/{{ $value->artist_name }}/{{ $value->album_name }}/{{ $value->music_title }}.mp3"/>
                <source id="song_played" src="/storage/musics/{{ $value->artist_name }}/{{ $value->album_name }}/{{ $value->music_title }}.wav"/>
                <source id="song_played" src="/storage/musics/{{ $value->artist_name }}/{{ $value->album_name }}/{{ $value->music_title }}.ogg"/>
                Your browser does not support the audio element.
            </audio>
        </div>
    </div>

@endsection
