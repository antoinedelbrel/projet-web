@extends('layout')

@section('content')

<div class="container mt-4">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Modifiez la playlist</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('playlists') }}">Retour</a>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('playlists.update',$playlist->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $playlist->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <input type="text" name="description" value="{{ $playlist->description }}" class="form-control" placeholder="Description">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Music id:</strong>
                    <input type="text" name="music_id" value="{{ $playlist->music_id }}" class="form-control" placeholder="music_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
</div>

@endsection
