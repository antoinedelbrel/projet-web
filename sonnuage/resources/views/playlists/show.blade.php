<?php

use Illuminate\Support\Facades\DB; ?>
@extends('layout')

@section('content')

<?php
$playlists = DB::table('playlists')
    ->select('playlists.id', 'playlists.name as playlist_name', 'playlists.description', 'playlists.owner_id', 'users.name as owner', 'musics.name as music_name')
    ->leftJoin('users', 'playlists.owner_id', '=', 'users.id')
    ->leftJoin('musics', 'users.id', '=', 'musics.artist_id')
    ->where('playlists.id', '=', $playlist->id)
    ->first();
?>
<div class="container mt-4">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Votre Playlist</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('playlists') }}">Retour</a>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $playlists->playlist_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Owner:</strong>
                {{ $playlists->owner }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $playlists->description }}
            </div>
        </div>
        <?php
        $musics = DB::table('playlists')
            ->select('musics.name as music_name', 'musics.id as music_id')
            ->leftJoin('musics', 'playlists.music_id', '=', 'musics.id')
            ->where('playlists.id', '=', $playlist->id)
            ->get();
        ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h3>Musics :</h3>
                @foreach($musics as $music)
                <a href="{{ route('musics.show', $music->music_id) }}">
                    <p>-> {{ $music->music_name }}</p>
                </a>
                @endforeach
            </div>
        </div>

    </div>

    @endsection