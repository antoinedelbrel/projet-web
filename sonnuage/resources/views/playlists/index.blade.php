@extends('layout')

<head>
    <title>SONUAGE - Playlists</title>
</head>

@section('content')

<?php

use Illuminate\Support\Facades\DB;
$playlists = DB::table('playlists')
    ->select('playlists.id', 'playlists.name', 'playlists.description', 'playlists.owner_id', 'users.name as owner')
    ->leftJoin('users', 'playlists.owner_id', '=', 'users.id')
    ->get();
?>
<div class="container mt-4">
    <div class="col-lg-12 margin-tb d-flex justify-content-between mb-2">
        <div class="pull-left">
            <h2>Toutes les playlists</h2>
        </div>
        <div class="pull-right">
            <a href="{{ route('playlists.create') }}" class="btn btn-success">Ajoutez une nouvelle playlist</a>
        </div>
    </div>

    <div class="group-playlists">
        @foreach($playlists as $playlist)
        <div class="playlist-unit">
            <a href="{{ route('playlists.show',$playlist->id) }}">
                <img src="{{ asset('images/playlist_white.png') }}" alt="playlist" />
                <h3>{{ $playlist->name }}</h3>
                <p>{{ $playlist->description }}</p>
                <p>De <strong>{{ $playlist->owner }}</strong></p>
            </a>
        </div>
        @endforeach
    </div>
    <h2>Vos playlists</h2>
    <div class="group-playlists">
        @foreach( $play = DB::table('playlists')->select('id', 'name', 'description')->where('owner_id', '=', (string)Auth::id())->get() as $playlists_user )
        <div class="playlist-unit">
            <img src="{{ asset('images/playlist_white.png') }}" alt="playlist" />
            <h3>{{ $playlists_user->name }}</h3>
            <p>{{ $playlists_user->description }}</p>
            <form action="{{ route('playlists.destroy',$playlists_user->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('playlists.show',$playlists_user->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('playlists.edit',$playlists_user->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
        @endforeach
    </div>
</div>
@endsection