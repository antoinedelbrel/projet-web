import ColorThief from "colorthief/dist/color-thief";

const ct = new ColorThief();
const img = document.querySelector('#img-cover');
let result = ct.getColor(img);

const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
    const hex = x.toString(16);
    return hex.length === 1 ? '0' + hex : hex
}).join('');

document.getElementById('cover').style.backgroundColor = rgbToHex(result[0], result[1], result[2]);
