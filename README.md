# Sonuage

## Description du projet

Nous avions pour objectif de faire un site web type spotify/deezer grâce à une API de deezer et nos données personnelles, mais nous nous sommes plutôt orienté vers un site comme soundcloud.
Le projet va être composer de :

* une base de donnée
* utilisation de librairie JavaScript/Laravel
* d’un système d’authentification

* des pages permettant à l'utilisateur de :
    * Gérer le catalogue
    * Modifier leur profil
    * Gérer leurs musiques préférées
    * Créer ses propres playlists et faire une liste de ses musiques préférés
    * Ajouter leur propres musiques
    * Créer leurs propres albums

* des fonctionnalités mettant en avant nos compétences en algorithmie

## Technologies utilisées 

Pour ce site nous avons utilisés comme technologies : 
 * Html
 * Scss
 * Javascript
 * Bootstrap
 * Laravel 

## Fonctionnalitées majeures 

* Inscription, connexion
* Upload de musique
* Ecoute de musique
* Ajout d'album
* Ajout de playlist 

## Commandes utilisées

* `laravel new` On crée un nouveau dossier laravel.
* `npm install` On installe les modules necessaires pour javascript et css.
* `php artisan make:model 'nom du model'` pour créer le model
* `php artisan make:controller 'nom du controller'` pour créer le controller 
* `php artisan make:migration 'nom de la table'` pour créer la table
* `php artisan make:seeder 'nom du seeder'` pour créer le seeder
* `php artisan make:factory 'nom de la factory'` pour créer la factory
* `composer require laravel/ui "^1.0" --dev`
* `php artisan ui vue --auth` ces deux commandes sont utilisées pour créer les pages d'authentification.
* `composer install` 
* `composer update`
* `npm run watch` Cette commande permet d'actualiser à chaque fois que du css où du javascript est changé pour voir la différence sur la page web.
* `php artisan migrate` pour migrer les tables
* `php artisan migrate:refresh --seed` pour refresh les migrations et rajouter les seeder
* `php artisan migrate:fresh --seed`
* `php artisan key:generate` générer une nouvelle clé
* `php artisan serve` pour lancer le serveur laravel